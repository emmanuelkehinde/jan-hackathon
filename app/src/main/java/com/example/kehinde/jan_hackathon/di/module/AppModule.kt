package com.example.kehinde.jan_hackathon.di.module

import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton
import com.example.kehinde.jan_hackathon.App
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings


@Module
class AppModule(var app: App) {

    @Provides
    @Singleton
    internal fun provideContext(): Context = app

    @Provides
    @Singleton
    fun provideFirebaseAuth(): FirebaseAuth = FirebaseAuth.getInstance()

    @Provides
    @Singleton
    fun provideFireStore(): FirebaseFirestore {
        val fireStore = FirebaseFirestore.getInstance()
        val fireStoreSettings = FirebaseFirestoreSettings.Builder().setPersistenceEnabled(true)
        fireStore.firestoreSettings = fireStoreSettings.build()
        return fireStore
    }

}