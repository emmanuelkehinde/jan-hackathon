package com.example.kehinde.jan_hackathon.ui.base

interface BasePresenter<in I: BaseView> {
    fun attachView(view: I)
    fun detachView()
}