package com.example.kehinde.jan_hackathon.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Message (
        var title: String,
        var body: String
) : Parcelable