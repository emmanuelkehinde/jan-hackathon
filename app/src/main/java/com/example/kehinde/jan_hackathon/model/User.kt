package com.example.kehinde.jan_hackathon.model

data class User (
        var userId: String = "",
        var fullName: String,
        var email: String
)