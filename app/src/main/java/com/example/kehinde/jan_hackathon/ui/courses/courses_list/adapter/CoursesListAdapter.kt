package com.example.kehinde.jan_hackathon.ui.courses.courses_list.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import com.example.kehinde.jan_hackathon.R
import com.example.kehinde.jan_hackathon.model.Course

class CoursesListAdapter(val context: Context, val repoListListener: CoursesListListener):
        RecyclerView.Adapter<CoursesListAdapter.RepoListVH>() {

    private var courses: ArrayList<Course> = ArrayList()

    interface CoursesListListener {
        fun onCourseClicked(position: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepoListVH {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_course_item,parent,false)
        return RepoListVH(view)
    }

    override fun getItemCount(): Int {
        return courses.size
    }

    override fun onBindViewHolder(holder: RepoListVH, position: Int) {
        holder.txtCourseName.text = courses[position].title
        holder.progressBar.progress = courses[position].progress
    }

    inner class RepoListVH(itemView: View?) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        var txtCourseName: TextView = itemView?.findViewById(R.id.txtCourseName)!!
        val progressBar: ProgressBar = itemView?.findViewById(R.id.progressBar)!!

        init {
            itemView?.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            repoListListener.onCourseClicked(adapterPosition)
        }
    }

    fun setCoursesListAndRefresh(courses: ArrayList<Course>) {
        this.courses = courses
        notifyDataSetChanged()
    }
}