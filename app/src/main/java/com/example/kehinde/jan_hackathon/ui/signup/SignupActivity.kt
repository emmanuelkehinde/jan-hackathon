package com.example.kehinde.jan_hackathon.ui.signup

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.kehinde.jan_hackathon.App
import com.example.kehinde.jan_hackathon.model.User
import com.example.kehinde.jan_hackathon.helper.showLongToast
import com.example.kehinde.jan_hackathon.helper.showToast
import com.example.kehinde.jan_hackathon.R
import com.example.kehinde.jan_hackathon.ui.custom.CustomProgress
import com.example.kehinde.jan_hackathon.ui.dashboard.DashboardActivity
import kotlinx.android.synthetic.main.sign_up.*
import javax.inject.Inject

class SignupActivity : AppCompatActivity(), SignupContract.SignupView {

    @Inject lateinit var signupPresenter: SignupPresenter
    private val progress: CustomProgress by lazy { CustomProgress(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.sign_up)

        App.getInstance().getDependencyComponent().inject(this)

        signupPresenter.attachView(this)

        signup_button.setOnClickListener {
            signupPresenter.createUser(User(
                    fullName = signup_name.text.toString(),
                    email = signup_email.text.toString()),
                    password = signup_password.text.toString()
            )
        }
    }

    override fun showProgress() {
        progress.show()
    }

    override fun hideProgress() {
        progress.dismiss()
    }

    override fun invalidPasswordError(string: String) {
        showLongToast(string)
    }

    override fun invalidEmailError(string: String) {
        showLongToast(string)
    }

    override fun onSignupError(string: String) {
        showLongToast("Error: $string")
    }

    override fun onSignupSuccess() {
        goToDashboardActivity()
    }

    private fun goToDashboardActivity() {
        startActivity(Intent(this, DashboardActivity::class.java))
        finish()
    }
}
