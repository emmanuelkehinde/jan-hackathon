package com.example.kehinde.jan_hackathon.helper

const val SPLASH_DISPLAY_LENGTH = 3000L
const val USERS = "users"
const val COURSES_KEY = "courses"
const val MESSAGE_KEY = "message"