package com.example.kehinde.jan_hackathon.ui.custom

import android.app.ProgressDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import com.example.kehinde.jan_hackathon.R


class CustomProgress(context: Context, private val isCancellable: Boolean = false) : ProgressDialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        setContentView(R.layout.progress_bar)
        setCancelable(isCancellable)
    }
}
