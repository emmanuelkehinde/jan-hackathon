package com.example.kehinde.jan_hackathon.ui.splash

import android.os.Handler
import com.example.kehinde.jan_hackathon.helper.SPLASH_DISPLAY_LENGTH
import com.example.kehinde.jan_hackathon.ui.splash.SplashContract

class SplashPresenter: SplashContract.SplashPresenter {

    private var splashView: SplashContract.SplashView? = null
    private val handler = Handler()

    override fun attachView(view: SplashContract.SplashView) {
        this.splashView = view
    }

    override fun detachView() {
        handler.removeCallbacksAndMessages(null)
        splashView = null
    }

    override fun startCountdown() {
        handler.postDelayed({
            splashView?.goToMainActivity()
        }, SPLASH_DISPLAY_LENGTH)
    }
}