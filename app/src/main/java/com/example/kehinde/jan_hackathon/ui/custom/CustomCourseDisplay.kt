package com.example.kehinde.jan_hackathon.ui.custom

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import com.example.kehinde.jan_hackathon.R
import com.example.kehinde.jan_hackathon.helper.COURSES_KEY
import com.example.kehinde.jan_hackathon.model.Course


class CustomCourseDisplay : DialogFragment() {

    private lateinit var course: Course

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        course = arguments!!.getParcelable(COURSES_KEY)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity!!)
        val custom = LayoutInflater.from(activity).inflate(R.layout.custom_display, null)

        builder.setView(custom)

        val courseCode = "${course.code}(${course.unit})"
        custom.findViewById<TextView>(R.id.course_code).text = courseCode
        custom.findViewById<TextView>(R.id.course_title).text = course.title
        custom.findViewById<TextView>(R.id.course_tutors).text = course.tutors
        custom.findViewById<TextView>(R.id.course_schedule).text = course.periods
        custom.findViewById<ProgressBar>(R.id.course_progress).progress = course.progress
        custom.findViewById<TextView>(R.id.course_registered).text =
                if (course.isRegistered) {
                    "Registered"
                } else "Not Registered"

        custom.findViewById<Button>(R.id.course_button).setOnClickListener {
            course.isRegistered = true
            custom.findViewById<TextView>(R.id.course_registered).text = getString(R.string.registered)
            custom.findViewById<Button>(R.id.course_button).visibility = View.INVISIBLE
        }

        return builder.create()
    }

    companion object {
        fun showCourseDetails(course: Course): CustomCourseDisplay = CustomCourseDisplay().apply {
            arguments = Bundle().apply {
                putParcelable(COURSES_KEY, course)
            }
        }
    }
}