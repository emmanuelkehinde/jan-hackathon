package com.example.kehinde.jan_hackathon.ui.login

import com.example.kehinde.jan_hackathon.ui.base.BasePresenter
import com.example.kehinde.jan_hackathon.ui.base.BaseView

interface LoginContract {

    interface LoginView: BaseView {
        fun showProgress()
        fun hideProgress()
        fun invalidPasswordError(string: String)
        fun invalidEmailError(string: String)
        fun onLoginError(string: String)
        fun onLoginSuccess()
        fun goToRegister()
    }

    interface LoginPresenter: BasePresenter<LoginView> {
        fun login(email: String, password: String)
        fun registerNow()
    }
}