package com.example.kehinde.jan_hackathon.ui.dashboard

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.example.kehinde.jan_hackathon.App
import com.example.kehinde.jan_hackathon.R
import com.example.kehinde.jan_hackathon.R.id.*
import com.example.kehinde.jan_hackathon.ui.courses.courses_list.CoursesListActivity
import com.example.kehinde.jan_hackathon.ui.custom.CustomProgress
import com.example.kehinde.jan_hackathon.ui.login.LoginActivity
import com.example.kehinde.jan_hackathon.ui.messages.MessagesActivity
import com.example.kehinde.jan_hackathon.ui.resources.ResourcesActivity
import com.example.kehinde.jan_hackathon.ui.surveys.survey_list.SurveysListActivity
import com.example.kehinde.jan_hackathon.ui.tutors.TutorsActivity
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.app_bar_dashboard.*
import kotlinx.android.synthetic.main.courses_card_view.*
import kotlinx.android.synthetic.main.surveys_card_view.*
import kotlinx.android.synthetic.main.tutors_card_view.*
import javax.inject.Inject

class DashboardActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener,
        DashboardContract.DashboardView {

    @Inject lateinit var dashboardPresenter: DashboardPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)

        App.getInstance().getDependencyComponent().inject(this)

        dashboardPresenter.attachView(this)

        setSupportActionBar(toolbar)
        supportActionBar?.title = getString(R.string.dashboard)

        val toggle = ActionBarDrawerToggle(this, drawer_layout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        courses_view_more.setOnClickListener { dashboardPresenter.onCoursesClicked() }

        tutors_view_more.setOnClickListener { dashboardPresenter.onTutorsClicked() }

        surveys_view_more.setOnClickListener { dashboardPresenter.onSurveysClicked() }
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.dashboard, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_signout -> {
                dashboardPresenter.signOut()
                startActivity(Intent(this, LoginActivity::class.java))
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_courses -> dashboardPresenter.onCoursesClicked()
            R.id.nav_tutors -> dashboardPresenter.onTutorsClicked()
            R.id.nav_surveys -> dashboardPresenter.onSurveysClicked()
            R.id.nav_messages -> dashboardPresenter.onMessagesClicked()
            R.id.nav_resources -> dashboardPresenter.onResourcesClicked()
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun goToCourseActivity() {
        startActivity(Intent(this, CoursesListActivity::class.java))
    }

    override fun goToTutorActivity() {
        startActivity(Intent(this, TutorsActivity::class.java))
    }

    override fun goToMessageActivity() {
        startActivity(Intent(this, MessagesActivity::class.java))
    }

    override fun goToSurveyActivity() {
        startActivity(Intent(this, SurveysListActivity::class.java))
    }

    override fun goToResourceActivity() {
        startActivity(Intent(this, ResourcesActivity::class.java))
    }

}
