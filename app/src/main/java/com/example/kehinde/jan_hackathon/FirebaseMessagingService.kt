package com.example.kehinde.jan_hackathon

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Intent
import android.content.Context
import android.media.RingtoneManager
import android.support.v4.app.NotificationCompat
import com.example.kehinde.jan_hackathon.helper.MESSAGE_KEY
import com.example.kehinde.jan_hackathon.ui.messages.MessagesActivity
import com.google.firebase.iid.FirebaseInstanceIdService
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage


class FirebaseMessagingService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage) {

    }

    private fun sendNotification(sentTime: Long, title: String, body: String) {
        val notificationBuilder = NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(body)
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setWhen(sentTime)

        val intent = Intent(this, MessagesActivity::class.java)
        intent.putExtra(MESSAGE_KEY, true)

        val pending = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
        notificationBuilder.setContentIntent(pending)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(0, notificationBuilder.build())
    }
}

class FirebaseInstanceIDService: FirebaseInstanceIdService() {
//    override fun onTokenRefresh() {
//        val refreshedToken = FirebaseInstanceId.getInstance().token
//        saveTokenToPrefs(refreshedToken)
//    }

//    private fun saveTokenToPrefs(token: String?) {
//        PreferenceManager.getDefaultSharedPreferences(this).edit().apply {
//            putString("registration_id", token) }.apply()
//    }
}

//fun sendTokenToServer(refreshedToken: String, userIDFks: String) {
//    val database = FirebaseDatabase.getInstance()
//    val myRef = database.getReference(TEST_ID)
//    myRef.child(userIDFks).child(myRef.push().key).setValue(refreshedToken)
//}
