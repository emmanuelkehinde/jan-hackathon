package com.example.kehinde.jan_hackathon.ui.courses.courses_list

import com.example.kehinde.jan_hackathon.helper.COURSES_KEY
import com.example.kehinde.jan_hackathon.model.Course
import com.google.firebase.firestore.FirebaseFirestore

class CoursesListPresenter(var firestore: FirebaseFirestore) : CoursesListContract.CoursesPresenter {

    override fun attachView(view: CoursesListContract.CoursesView) {
        this.coursesListView = view
    }

    override fun detachView() {
        this.coursesListView = null
    }

    var coursesListView: CoursesListContract.CoursesView? = null

    override fun fetchCourses() {
        val courses = arrayListOf<Course>()
        firestore.collection("courses")
                .get()
                .addOnSuccessListener {
                    it.forEach {
                        courses.add(it.toObject(Course::class.java))
                    }
                }.addOnFailureListener {
                    this.coursesListView?.onCoursesFetchFailure(it)
                }
        this.coursesListView?.onCoursesFetchSuccess(courses)
    }
}