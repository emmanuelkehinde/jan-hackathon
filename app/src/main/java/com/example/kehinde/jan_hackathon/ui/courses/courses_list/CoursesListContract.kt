package com.example.kehinde.jan_hackathon.ui.courses.courses_list

import com.example.kehinde.jan_hackathon.model.Course
import com.example.kehinde.jan_hackathon.ui.base.BasePresenter
import com.example.kehinde.jan_hackathon.ui.base.BaseView
import java.lang.Exception

interface CoursesListContract {

    interface CoursesView: BaseView {
        fun onCoursesFetchSuccess(courses: ArrayList<Course>)
        fun onCoursesFetchFailure(it: Exception)
    }

    interface CoursesPresenter: BasePresenter<CoursesView> {
        fun fetchCourses()
    }
}