package com.example.kehinde.jan_hackathon.ui.login

import com.example.kehinde.jan_hackathon.helper.isValidEmail
import com.example.kehinde.jan_hackathon.helper.isValidPassword
import com.example.kehinde.jan_hackathon.ui.login.LoginContract
import com.google.firebase.auth.FirebaseAuth

class LoginPresenter(private var mAuth: FirebaseAuth): LoginContract.LoginPresenter {

    private var loginView: LoginContract.LoginView? = null

    override fun attachView(view: LoginContract.LoginView) {
        this.loginView = view
    }

    override fun detachView() {
        this.loginView = null
    }

    override fun login(email: String, password: String) {
        if (!email.isValidEmail()) {
            loginView?.invalidEmailError("Enter a valid email.")
            return
        }

        if (!password.isValidPassword()) {
            loginView?.invalidPasswordError("Enter a valid password.")
            return
        }

        loginView?.showProgress()

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener { result ->
                    loginView?.hideProgress()

                    if (result.isSuccessful){
                        loginView?.onLoginSuccess()
                    } else {
                        loginView?.onLoginError(result.exception?.message!!)
                    }
                }
    }

    override fun registerNow() {
        loginView?.goToRegister()
    }
}