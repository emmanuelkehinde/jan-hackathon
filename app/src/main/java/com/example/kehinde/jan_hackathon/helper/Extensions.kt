package com.example.kehinde.jan_hackathon.helper

import android.app.Activity
import android.content.Context
import android.view.Window
import android.widget.Toast

fun Activity.addFullScreenParameters() {
    this.requestWindowFeature(Window.FEATURE_NO_TITLE)
    window.setFlags(android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN,
            android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN)
}

fun Context.showToast(message: String)
        = Toast.makeText(this, message, android.widget.Toast.LENGTH_SHORT).show()

fun Context.showLongToast(message: String)
        = Toast.makeText(this, message, Toast.LENGTH_LONG).show()

fun String.isValidEmail()
        = this.matches(Regex("([a-zA-Z0-9.-_]+)@([a-zA-Z0-9-]+)\\.(([a-zA-Z0-9.]){2,})"))

fun String.isValidPassword()
        = this.length > 3