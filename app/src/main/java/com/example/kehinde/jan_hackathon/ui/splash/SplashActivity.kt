package com.example.kehinde.jan_hackathon.ui.splash

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.kehinde.jan_hackathon.helper.addFullScreenParameters
import com.example.kehinde.jan_hackathon.ui.login.LoginActivity
import com.example.kehinde.jan_hackathon.ui.signup.SignupActivity

class SplashActivity : AppCompatActivity(), SplashContract.SplashView {

    private lateinit var splashPresenter: SplashPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        addFullScreenParameters()
        super.onCreate(savedInstanceState)

        splashPresenter = SplashPresenter()
        splashPresenter.attachView(this)
        splashPresenter.startCountdown()
    }

    override fun goToMainActivity() {
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }

    override fun onDestroy() {
        super.onDestroy()
        splashPresenter.detachView()
    }
}
