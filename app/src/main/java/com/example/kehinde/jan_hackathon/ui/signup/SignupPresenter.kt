package com.example.kehinde.jan_hackathon.ui.signup

import com.example.kehinde.jan_hackathon.model.User
import com.example.kehinde.jan_hackathon.helper.USERS
import com.example.kehinde.jan_hackathon.helper.isValidEmail
import com.example.kehinde.jan_hackathon.helper.isValidPassword
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class SignupPresenter(private var mAuth: FirebaseAuth, private var mStore: FirebaseFirestore):
        SignupContract.SignupPresenter {

    private var signupView: SignupContract.SignupView? = null

    override fun attachView(view: SignupContract.SignupView) {
        this.signupView = view
    }

    override fun detachView() {
        signupView = null
    }

    override fun createUser(user: User, password: String) {
        if (!user.email.isValidEmail()) {
            signupView?.invalidEmailError("Enter a valid email.")
            return
        }

        if (!password.isValidPassword()) {
            signupView?.invalidPasswordError("Enter a valid password.")
            return
        }

        signupView?.showProgress()

        mAuth.createUserWithEmailAndPassword(user.email, password)
                .addOnCompleteListener { result ->
                    signupView?.hideProgress()
                    if (result.isSuccessful) {
                        user.userId = mAuth.currentUser?.uid!!
                        createUserInDb(user)
                    } else {
                        signupView?.onSignupError(result.exception?.message!!)
                    }
                }
    }

    private fun createUserInDb(user: User) {
        mStore.collection(USERS)
                .document(mAuth.currentUser?.uid!!)
                .set(user)
                .addOnSuccessListener {
                    signupView?.onSignupSuccess()
                }
                .addOnFailureListener { error ->
                    signupView?.onSignupError(error.message.toString())
                }
    }

}