package com.example.kehinde.jan_hackathon.model

import android.os.Parcel
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Course (
        var code: String,
        var title: String,
        var unit: Int,
        var progress: Int,
        var tutors: String,
        var periods: String,
        var isRegistered: Boolean
) : Parcelable