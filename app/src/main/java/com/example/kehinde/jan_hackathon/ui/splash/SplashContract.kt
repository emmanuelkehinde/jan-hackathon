package com.example.kehinde.jan_hackathon.ui.splash

import com.example.kehinde.jan_hackathon.ui.base.BasePresenter
import com.example.kehinde.jan_hackathon.ui.base.BaseView

interface SplashContract {

    interface SplashView: BaseView {
        fun goToMainActivity()
    }

    interface SplashPresenter: BasePresenter<SplashView> {
        fun startCountdown()
    }
}