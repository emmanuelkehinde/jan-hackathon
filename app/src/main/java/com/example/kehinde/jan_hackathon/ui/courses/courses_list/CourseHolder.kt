package com.emmanuelkehinde.jobb.views.partials

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import com.example.kehinde.jan_hackathon.model.Course
import kotlinx.android.synthetic.main.layout_course_item.view.*
import java.util.*

class CourseHolder(itemView: View, private var courseListener: CourseListener): RecyclerView.ViewHolder(itemView), View.OnClickListener {

    private lateinit var model: Course
    private var courseName: TextView = itemView.txtCourseName
    private var courseProgress: ProgressBar = itemView.progressBar


    interface CourseListener {
        fun onCourseClicked(course: Course)
    }

    init {
        itemView.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        courseListener.onCourseClicked(model)
    }

    fun bind(model: Course) {
        this.model = model
        this.courseName.text = model.title
        this.courseProgress.progress = model.progress
    }


}