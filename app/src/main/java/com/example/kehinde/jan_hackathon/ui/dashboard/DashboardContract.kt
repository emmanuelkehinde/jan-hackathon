package com.example.kehinde.jan_hackathon.ui.dashboard

import com.example.kehinde.jan_hackathon.ui.base.BasePresenter
import com.example.kehinde.jan_hackathon.ui.base.BaseView

interface DashboardContract {

    interface DashboardView: BaseView {
        fun goToCourseActivity()
        fun goToTutorActivity()
        fun goToMessageActivity()
        fun goToSurveyActivity()
        fun goToResourceActivity()
    }

    interface DashboardPresenter: BasePresenter<DashboardView> {
        fun onCoursesClicked()
        fun onTutorsClicked()
        fun onMessagesClicked()
        fun onSurveysClicked()
        fun onResourcesClicked()
        fun signOut()
    }
}