package com.example.kehinde.jan_hackathon.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Tutor (
        var name: String,
        var title: String,
        var rating: Int
) : Parcelable