package com.example.kehinde.jan_hackathon.ui.courses.courses_list

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.ViewGroup
import com.emmanuelkehinde.jobb.views.partials.CourseHolder
import com.example.kehinde.jan_hackathon.App
import com.example.kehinde.jan_hackathon.R
import com.example.kehinde.jan_hackathon.helper.COURSES_KEY
import com.example.kehinde.jan_hackathon.helper.showToast
import com.example.kehinde.jan_hackathon.model.Course
import com.example.kehinde.jan_hackathon.ui.courses.courses_detail.CourseDetailActivity
import com.example.kehinde.jan_hackathon.ui.courses.courses_list.adapter.CoursesListAdapter
//import com.firebase.ui.firestore.FirestoreRecyclerAdapter
//import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import kotlinx.android.synthetic.main.activity_courses.*
import java.lang.Exception
import javax.inject.Inject

class CoursesListActivity : AppCompatActivity(), CoursesListContract.CoursesView, CoursesListAdapter.CoursesListListener {

    private var courses: ArrayList<Course> = ArrayList()
    private lateinit var coursesListAdapter: CoursesListAdapter

    @Inject
    lateinit var coursesListListPresenter: CoursesListPresenter

    @Inject
    lateinit var fireStore: FirebaseFirestore

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_courses)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.courses)

        App.getInstance().getDependencyComponent().inject(this)

        setUpRecyclerView()

        courses.add(Course("CSC 201", "Testing", 3, 60, "Dr. Aina", "Mondays, 9am - 11 am", true))
        courses.add(Course("CSC 201", "Testing", 3, 60, "Dr. Aina", "Mondays, 9am - 11 am", false))
        courses.add(Course("CSC 201", "Testing", 3, 60, "Dr. Aina", "Mondays, 9am - 11 am", false))
        courses.add(Course("CSC 201", "Testing", 3, 60, "Dr. Aina", "Mondays, 9am - 11 am", true))

        displayCourses(courses)

    }

    private fun setUpRecyclerView() {
        recyclerCourseList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false)
        recyclerCourseList.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        coursesListAdapter = CoursesListAdapter(this,this)
        recyclerCourseList.adapter = coursesListAdapter
    }

    private fun displayCourses(courses: ArrayList<Course>) {
        coursesListAdapter.setCoursesListAndRefresh(courses)
    }

    override fun onCoursesFetchSuccess(courses: ArrayList<Course>) {
        displayCourses(courses)
    }

    override fun onCoursesFetchFailure(it: Exception) {
        showToast(it.localizedMessage)
    }

    override fun onCourseClicked(position: Int) {
        startActivity(Intent(this, CourseDetailActivity::class.java))
    }

//    private fun getCoursesAdapter(): FirestoreRecyclerAdapter<Course, CourseHolder> {
//        val query: Query = fireStore.collection("courses")
//        val options = FirestoreRecyclerOptions.Builder<Course>()
//                .setQuery(query, Course::class.java)
//                .build()
//        val adapter = object : FirestoreRecyclerAdapter<Course, CourseHolder>(options), CourseHolder.CourseListener {
//            override fun onCourseClicked(course: Course) {
//
//            }
//
//
//            override fun onBindViewHolder(holder: CourseHolder, position: Int, model: Course) {
//                holder.bind(model)
//            }
//
//            override fun onCreateViewHolder(group: ViewGroup, i: Int): CourseHolder {
//                // Create a new instance of the ViewHolder, in this case we are using a custom
//                // layout called R.layout.message for each item
//                val view = LayoutInflater.from(group.context)
//                        .inflate(R.layout.layout_course_item, group, false)
//
//                return CourseHolder(view, this)
//            }
//        }
//
//        return adapter
//    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}
