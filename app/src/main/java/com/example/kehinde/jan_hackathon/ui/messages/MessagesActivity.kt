package com.example.kehinde.jan_hackathon.ui.messages

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.kehinde.jan_hackathon.R

class MessagesActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_message)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}
