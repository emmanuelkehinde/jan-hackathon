package com.example.kehinde.jan_hackathon.di.component

import com.example.kehinde.jan_hackathon.di.module.AppModule
import com.example.kehinde.jan_hackathon.di.module.DependencyModule
import com.example.kehinde.jan_hackathon.ui.dashboard.DashboardActivity
import com.example.kehinde.jan_hackathon.ui.courses.courses_list.CoursesListActivity
import com.example.kehinde.jan_hackathon.ui.login.LoginActivity
import com.example.kehinde.jan_hackathon.ui.signup.SignupActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(AppModule::class), (DependencyModule::class)])
interface DependencyComponent {
    fun inject(signupActivity: SignupActivity)
    fun inject(loginActivity: LoginActivity)
    fun inject(dashboardActivity: DashboardActivity)
    fun inject(coursesListActivity: CoursesListActivity)
}