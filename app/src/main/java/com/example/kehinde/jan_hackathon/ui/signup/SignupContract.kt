package com.example.kehinde.jan_hackathon.ui.signup

import com.example.kehinde.jan_hackathon.model.User
import com.example.kehinde.jan_hackathon.ui.base.BasePresenter
import com.example.kehinde.jan_hackathon.ui.base.BaseView

interface SignupContract {

    interface SignupView: BaseView {
        fun showProgress()
        fun hideProgress()
        fun invalidPasswordError(string: String)
        fun invalidEmailError(string: String)
        fun onSignupError(string: String)
        fun onSignupSuccess()
    }

    interface SignupPresenter: BasePresenter<SignupView> {
       fun createUser(user: User, password: String)
    }
}