package com.example.kehinde.jan_hackathon.di.module


import com.example.kehinde.jan_hackathon.ui.dashboard.DashboardPresenter
import com.example.kehinde.jan_hackathon.ui.courses.courses_list.CoursesListPresenter
import com.example.kehinde.jan_hackathon.ui.login.LoginPresenter
import com.example.kehinde.jan_hackathon.ui.signup.SignupPresenter
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DependencyModule {

    @Provides
    @Singleton
    fun provideSignupPresenter(firebaseAuth: FirebaseAuth, firestore: FirebaseFirestore)
            = SignupPresenter(firebaseAuth, firestore)

    @Provides
    @Singleton
    fun provideLoginPresenter(firebaseAuth: FirebaseAuth) = LoginPresenter(firebaseAuth)

    @Provides
    @Singleton
    fun provideDashboardPresenter(firebaseAuth: FirebaseAuth) = DashboardPresenter(firebaseAuth)

    @Provides
    @Singleton
    fun provideCoursesPresenter(firestore: FirebaseFirestore) = CoursesListPresenter(firestore)

}