package com.example.kehinde.jan_hackathon.ui.surveys.survey_form

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.kehinde.jan_hackathon.R

class SurveyFormActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_survey_form)
    }
}
