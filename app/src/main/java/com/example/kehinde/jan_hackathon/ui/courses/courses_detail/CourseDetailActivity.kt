package com.example.kehinde.jan_hackathon.ui.courses.courses_detail

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.kehinde.jan_hackathon.R

class CourseDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_course_detail)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }
}
