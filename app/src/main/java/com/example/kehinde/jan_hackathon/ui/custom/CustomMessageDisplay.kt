package com.example.kehinde.jan_hackathon.ui.custom

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.widget.TextView
import com.example.kehinde.jan_hackathon.R
import com.example.kehinde.jan_hackathon.model.Message


class CustomMessageDisplay : DialogFragment() {

    private lateinit var message: Message

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        message = arguments!!.getParcelable("message")
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity!!)
        val custom = LayoutInflater.from(activity).inflate(R.layout.message, null)

        builder.setView(custom)

        custom.findViewById<TextView>(R.id.message_title).text = message.title
        custom.findViewById<TextView>(R.id.message_body).text = message.body

        return builder.create()
    }

    companion object {
        fun showMessageDetails(message: Message): CustomMessageDisplay = CustomMessageDisplay().apply {
            arguments = Bundle().apply {
                putParcelable("message", message)
            }
        }
    }
}