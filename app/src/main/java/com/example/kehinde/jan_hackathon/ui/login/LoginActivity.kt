package com.example.kehinde.jan_hackathon.ui.login

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.kehinde.jan_hackathon.App
import com.example.kehinde.jan_hackathon.R
import com.example.kehinde.jan_hackathon.R.id.login_button
import com.example.kehinde.jan_hackathon.helper.showLongToast
import com.example.kehinde.jan_hackathon.ui.custom.CustomProgress
import com.example.kehinde.jan_hackathon.ui.dashboard.DashboardActivity
import com.example.kehinde.jan_hackathon.ui.signup.SignupActivity
import kotlinx.android.synthetic.main.log_in.*
import javax.inject.Inject

class LoginActivity : AppCompatActivity(), LoginContract.LoginView {

    @Inject lateinit var loginPresenter: LoginPresenter
    private val progress: CustomProgress by lazy { CustomProgress(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.log_in)

        App.getInstance().getDependencyComponent().inject(this)

        loginPresenter.attachView(this)

        login_button.setOnClickListener {
            loginPresenter.login(login_email.text.toString(), login_password.text.toString())
        }

        registerNow.setOnClickListener {
            loginPresenter.registerNow()
        }

    }

    override fun goToRegister() {
        startActivity(Intent(this,SignupActivity::class.java))
    }

    override fun showProgress() {
        progress.show()
    }

    override fun hideProgress() {
        progress.dismiss()
    }

    override fun invalidPasswordError(string: String) {
        showLongToast(string)
    }

    override fun invalidEmailError(string: String) {
        showLongToast(string)
    }

    override fun onLoginError(string: String) {
        showLongToast("Error: $string")
    }

    override fun onLoginSuccess() {
        goToDashboardActivity()
    }

    private fun goToDashboardActivity() {
        startActivity(Intent(this, DashboardActivity::class.java))
        finish()
    }
}