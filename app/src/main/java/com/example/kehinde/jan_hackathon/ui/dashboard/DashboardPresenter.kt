package com.example.kehinde.jan_hackathon.ui.dashboard

import com.google.firebase.auth.FirebaseAuth


class DashboardPresenter(private var mAuth: FirebaseAuth): DashboardContract.DashboardPresenter {
    private var dashboardView: DashboardContract.DashboardView? = null

    override fun attachView(view: DashboardContract.DashboardView) {
        this.dashboardView = view
    }

    override fun detachView() {
        this.dashboardView = null
    }

    override fun onCoursesClicked() {
        dashboardView?.goToCourseActivity()
    }

    override fun signOut() {
        mAuth.signOut()
    }

    override fun onTutorsClicked() {
        dashboardView?.goToTutorActivity()
    }

    override fun onMessagesClicked() {
        dashboardView?.goToMessageActivity()
    }

    override fun onSurveysClicked() {
        dashboardView?.goToSurveyActivity()
    }

    override fun onResourcesClicked() {
        dashboardView?.goToResourceActivity()
    }

}