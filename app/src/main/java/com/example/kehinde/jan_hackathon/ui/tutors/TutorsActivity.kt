package com.example.kehinde.jan_hackathon.ui.tutors

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.kehinde.jan_hackathon.R

class TutorsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tutors)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}
